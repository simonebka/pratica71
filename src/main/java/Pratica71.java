/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

public class Pratica71 {

  public static void main(String[] args) {
    Time time = new Time();
    
    // este Scanner lerá dados da entrada padrão, em geral, do teclado
    Scanner scanner = new Scanner(System.in);
    int numj=0, k=0, l=0, achou=0;
    String posjog, nomejog;
    System.out.print("Informe o número de jogadores: ");
    numj = scanner.nextInt();
    // adicionando jogadores ao time
    while(k<numj)
    {
        System.out.print("Informe o número do jogador: ");
        int jog = scanner.nextInt();// tratar erro de não conversão
        if (jog>0)
        {
            System.out.print("Informe a posição do jogador: ");
            posjog = scanner.next();   
            System.out.print("Informe o nome do jogador: ");
            nomejog = scanner.next();   
            time.addJogador(posjog, new Jogador(jog, nomejog));
            k++;
        }
        else {
            System.out.println(jog + " não é um número");
            }
        
    }
         
    // ordem ascendente de número
    JogadorComparator ordem = new JogadorComparator(true, true, false);//(false, true, false);
    List<Jogador> timeA = time.ordena(ordem);  
    
    timeA.forEach((jogador) -> {
        System.out.println(jogador.toString());
      });
    l=-1;
    while(l!=0)
    {
        System.out.print("Informe o número do jogador: ");
        String jog1 = scanner.next();// tratar erro de não conversão
        int jog = Integer.parseInt(jog1);
        if(jog==0) break;
        /*if (jog>0)
        {
           System.out.print("Informe o nome do jogador: ");
           String jognome = scanner.next();// tratar erro de não conversão           
           Jogador novo = new Jogador(jog, jognome); 
            boolean posdup=timeA.contains(novo);
            if(!posdup)//posdup<0)
            {
                System.out.print(posdup);
                //System.out.print("Informe o nome do jogador: ");
                //nomejog = scanner.next();   
                timeA.add(novo);
            }
            else
            {
                System.out.print(posdup);
                System.out.print("Jogador já existe!");
            }
         }
                          
        else {
            System.out.println(" FIM!");
            break;
            }*/
        achou=0;
        for (Jogador j: timeA) {
        int x=j.getNumero();
        if(x==jog)
        {
           System.out.println("Jogador já existe!");
           System.out.println("Atualizar nome do jogador:");
           nomejog = scanner.next();
           j.setNome(nomejog);
           achou=1;
           break;
        }
        /*else
            System.out.println("Cadastrar!");*/
        }
       if(achou==0)
       {
           System.out.print("Informe o nome do jogador: ");
           nomejog = scanner.next();  
           timeA.add(new Jogador(jog,nomejog));
           System.out.println("Jogador inserido com sucesso!");
       }
       else
           System.out.println("Cadastro atualizado!");
       
       JogadorComparator ordem1 = new JogadorComparator(true, true, false);
       timeA = time.ordena(ordem1);
        timeA.forEach((jogador) -> {
          System.out.println(jogador);
        });
    }
            
        
    }
    
  }
    
